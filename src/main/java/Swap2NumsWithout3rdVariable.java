public class Swap2NumsWithout3rdVariable {
    public static void main(String[] args) {
        int num1 = 46;
        int num2 = 36;

        System.out.println("Num1 before swapping is: " + num1);
        System.out.println("Num2 before swapping is: " + num2);

        num1 = num1 + num2;
        num2 = num1 - num2;
        num1 = num1 - num2;

        System.out.println("Num1 before swapping is: " + num1);
        System.out.println("Num2 before swapping is: " + num2);
    }
}
