import java.util.HashMap;

public class CountWordsUsingHashMap {
    public static void main(String[] args) {
        String msg = "Hi I I will be available Hi after 5 pm";

        String[] msgArray = msg.split(" ");

        HashMap<String, Integer> hashMap = new HashMap<>();

        for (String s : msgArray) {
            if (hashMap.containsKey(s)) {
                int count = hashMap.get(s);
                hashMap.put(s, count + 1);
            } else {
                hashMap.put(s, 1);
            }
        }
        System.out.println(hashMap);
    }
}
