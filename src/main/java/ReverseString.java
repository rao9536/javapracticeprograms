public class ReverseString {
    public static void main(String[] args) {
        String str ="Hello";
        StringBuilder stringBuilder = new StringBuilder(str);
        System.out.println(stringBuilder.reverse());
    }
}
