import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddDigits {

    public static void main(String[] args) {
        System.out.println("Enter a number ");

        // Getting input from User by using the Scanner class
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();

        // Validating input that it is not carrying any string or characters
        boolean isValidInput = verifyDigits(input);

        if (isValidInput) {

            //adding digits and then printing the result directly from System out
            System.out.println("Sum of First 10 digits is : " + checkAndDigits(input));
        } else {

            //Throwing the exception if input is carrying any string or characters OR input is empty
            throw new IllegalArgumentException("Input is Invalid..May be having characters or no input given");
        }
    }

    public static int checkAndDigits(String digits) {

        //getting the only first 10 digits from input
        if (digits.length() > 10) {
            digits = digits.substring(0, Math.min(digits.length(), 10));
        }

        int length = digits.length();

        int sum = 0;
        boolean isSecond = false;

        //replacing every second digit with its double value and then adding to sum variable
        for (int i = length - 1; i >= 0; i--) {
            int d = digits.charAt(i) - '0';
            if (isSecond == true)
                d = d * 2;
            sum += d / 10;
            sum += d % 10;
            isSecond = !isSecond;
        }
        return sum;
    }

    public static boolean verifyDigits(String input) {
        String regex = "[0-9]+";

        //applying the pattern to check input is not having any characters or string
        Pattern pattern = Pattern.compile(regex);
        if (input == null) {
            return false;
        }
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }
}
