import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

public class OpenAllGmailLinks {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "E:\\Selenium Browser Drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.gmail.com");
        List<WebElement> webElementList = driver.findElements(By.tagName("a"));
        for(WebElement element : webElementList){
            System.out.println(element.getAttribute("href"));
            System.out.println(element.getText());
        }
        driver.close();
    }
}
