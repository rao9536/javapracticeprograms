import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class FibonacciSeries {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        scanner.close();

        List<Integer> fibonncci = new LinkedList();
        fibonncci.add(0);
        fibonncci.add(1);
        for(int i=1;i<=num-2;i++){
            int sumOfLast2Digits = fibonncci.get(i-1)+fibonncci.get(i);
            fibonncci.add(sumOfLast2Digits);
        }
        System.out.println("Fibonacci Series of "+num+" is :\n"+fibonncci);
    }
}
