import java.util.ArrayList;
import java.util.List;

public class DuplicateCharsInString {
    public static void main(String[] args) {
        String str = "Hello hi Hello how are you?";
        String[] stringArray = str.split(" ");
        List<String> list = new ArrayList<>();
        List<String> dupList = new ArrayList<>();
        for (int i = 0; i < stringArray.length; i++) {
            if (!list.isEmpty()) {
                if (list.contains(stringArray[i])) {
                    dupList.add(stringArray[i]);
                } else {
                    list.add(stringArray[i]);
                }
            } else {
                list.add(stringArray[i]);
            }
        }
        System.out.println(dupList);
    }
}
