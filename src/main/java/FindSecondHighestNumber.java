public class FindSecondHighestNumber {
    public static void main(String[] args) {
        Integer[] numbers = {30, 50, 25, 43, 80, 56, 90, 88, 89};
        int targetNumber = 0, maxNumber = 0, secondMax = 0;
        secondMax = numbers[0];
        maxNumber = numbers[0];
        for (int i = 0; i <= numbers.length - 1; i++) {
            if (numbers[i] > maxNumber) {
                secondMax = maxNumber;
                maxNumber = numbers[i];
            } else if (numbers[i] < maxNumber && numbers[i] > secondMax) {
                secondMax = numbers[i];
            }
        }
        System.out.println("Second largest number is : " + secondMax);
        System.out.println("Largest number is : " + maxNumber);
    }
}
