import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class ScrollUpDown {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "E:\\Selenium Browser Drivers\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        //driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://www.google.com");
        WebElement element = driver.findElement(By.xpath("//body/div[1]/div[3]/form[1]/div[2]/div[1]/div[1]/div[1]/div[2]/input[1]"));
        element.click();
        element.sendKeys("SoftwareTestingHelp");
        element.sendKeys(Keys.ENTER);
        try{
            js.executeScript("window.scrollBy(0,2000)");
            Thread.sleep(10000);
            js.executeScript("window.scrollTo(0,document.body.scrollHeight)");
            Thread.sleep(10000);
        }catch (Exception e){
            e.printStackTrace();
        }
        driver.close();
    }
}
