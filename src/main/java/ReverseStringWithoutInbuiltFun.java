public class ReverseStringWithoutInbuiltFun {

    public static void main(String[] args) {
        String str = "Hello Java";
        StringBuilder reverseStr = new StringBuilder();
        char[] chars = str.toCharArray();

        for (int i = chars.length-1; i>=0; i--) {
            reverseStr.append(chars[i]);
        }
        System.out.println(reverseStr);
    }
}
