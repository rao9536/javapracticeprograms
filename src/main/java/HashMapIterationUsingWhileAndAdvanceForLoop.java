import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HashMapIterationUsingWhileAndAdvanceForLoop {
    public static void main(String[] args) {
        HashMap<Integer, String> dataMap = new HashMap<>();
        dataMap.put(1,"Ramesh");
        dataMap.put(2,"Basil");
        dataMap.put(3,"Karthik");
        dataMap.put(4,"Aaditya");
        dataMap.put(5,"Aman");
        dataMap.put(6,"Balwinder");
        Iterator itr = dataMap.entrySet().iterator();

        while (itr.hasNext()){
            Map.Entry entry = (Map.Entry) itr.next();
            System.out.println("Room number is : "+entry.getKey()+"\nName is : "+entry.getValue());
        }

        for(Map.Entry entry : dataMap.entrySet()){
            System.out.println("Room number is : "+entry.getKey()+"\nName is : "+entry.getValue());
        }

    }
}
