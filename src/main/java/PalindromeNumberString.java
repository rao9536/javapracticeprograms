import java.util.Scanner;

public class PalindromeNumberString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        scanner.close();
        String reversed;

        StringBuilder stringBuilder = new StringBuilder(input);
        stringBuilder.reverse();
        reversed = stringBuilder.toString();

        if(input.equals(reversed)){
            System.out.println(input+" is Palindrome");
        }else{
            System.out.println(input+" is  not Palindrome");
        }
    }
}
