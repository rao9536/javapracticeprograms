import java.util.Scanner;

public class NumberIsPrimeOrNot {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        scanner.close();
        int result = num / 2;
        boolean flag = true;
        if (num == 0 || num == 1) {
            System.out.println(num + " is not a prime number");
        } else {
            for(int i=2; i<=result;i++){
                if (num % i == 0) {
                    System.out.println(num + " is not a prime number");
                    flag = false;
                    break;
                }
            }
             if(flag) {
                System.out.println(num + " is a prime number");
            }
        }
    }
}
