public class Swap2Numbers {
    public static void main(String[] args) {
        int num1 = 100;
        int num2 = 200;
        int temp;

        System.out.println("Num1 before swapping is: "+num1);
        System.out.println("Num2 before swapping is: "+num2);
        temp = num1;
        num1 = num2;
        num2 = temp;

        System.out.println("Num1 after swapping is: "+num1);
        System.out.println("Num2 after swapping is: "+num2);
    }
}
